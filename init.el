;; setup repositories
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/")
	     '("org" . "https://orgmode.org/elpa/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Enable use of y or n instead of yes or no
(defalias 'yes-or-no-p 'y-or-n-p)
(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(add-to-list 'load-path "~/.emacs.d/config")
(setq custom-safe-themes t)
(require 'ui)
(require 'evil-mode)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (evil-collection evil-surround org-evil evil use-package smart-mode-line flycheck dashboard base16-theme))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
