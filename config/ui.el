;; ui --- Modeline replacement
;;; Commentary: All the config related to look and feel
;;; Code:
(use-package smart-mode-line
  :ensure t
  :init
  (sml/setup)
  :config
  (setq sml/theme 'respectful))

;; install a nice colorscheme
(use-package base16-theme
  :ensure t
  :config (setq custom-safe-themes t)
  :init (load-theme 'base16-embers t nil))
  
  
;; nicer startup screen
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-startup-banner 'logo))

;; disable default one
(setq inhibit-startup-screen t)

;; scroll line per line (feel smoother to me)
(setq scroll-conservatively 100)

;; highlight current line
(when window-system (global-hl-line-mode))

;; disable useless GUI element
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

;; set fonts
;; default one
(set-frame-font "Dina:pixelsize=13:antialias=false:hinting:false" nil t)
;; japanese font
(set-fontset-font t 'japanese-jisx0208 (font-spec :name "-shinonome-mincho-medium-r-normal--16-150-75-75-c-160-jisx0208.1990-0")) 

(provide 'ui)
;;; ui.el ends here
