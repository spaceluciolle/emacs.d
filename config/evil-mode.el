;;; evil-mode --- configuration for evil and is plugins
;;; Commentary: Configure evil modes

;;; Code: load evil
(use-package evil
  :ensure t
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  :init (evil-mode))

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))

(use-package org-evil
  :ensure t)				;

(provide 'evil-mode)
;;; evil-mode ends here
